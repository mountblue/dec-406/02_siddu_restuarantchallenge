const csv = require('csvtojson')
let csv_filename = './restaurant.csv';
let search_datetime = ""
var readline = require('readline');
var rl = readline.createInterface(process.stdin, process.stdout);
rl.question("Type in the fileName: ", function (fileName) {
    csv_filename = './' + fileName;
    rl.question("Type the input in dd-mm-yy with time:", function (data) {
        console.log(csv_filename);
        find_open_restaurants(csv_filename, data);
    })
})

function find_open_restaurants(csv_filename, search_datetime) {
    let restuarantData = [];
    let temp;
    let date = new Date(search_datetime.split(' ')[0]);
    let time = search_datetime.split(' ')[1];
    let restaurants;
    time = convertTimeToMinute(time);
    // if there is no proper time return 
    if (time === undefined) {
        console.log("Please give the correct time input..");
        return "";
    }
    // using csvparser to convert to json format
    csv()
        .fromFile(csv_filename)
        .then(jsonData => {
            // convert the data into dictionary by restuarant name as  key and its timing as value
            jsonData.forEach(data => {
                temp = {};
                temp[Object.values(data)[0]] = Object.values(data)[1];
                restuarantData.push(temp);
            })
            restaurants = getRestuarantsByDayTime(restuarantData, date.getDay(), time)
            console.log(restaurants);
        })
}

function getRestuarantsByDayTime(restuarantData, presentDay, presentTime) {
    let weekDay = ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"]
    let temp = "";
    let time = "";
    let days = [];
    let start, end;
    let restaurants = [];
    for (var x in restuarantData) {
        for (var y in restuarantData[x]) {
            // split the restuarant based on their timings
            temp = restuarantData[x][y].split('/');
            temp.forEach(data => {
                // get timing of the particular day using regex..
                time = (data.match(/[(0-9: (ampm)]+/ig))
                time = time.filter(t => t.length >= 4);
                time.forEach((t, index) => {
                    time[index] = convertTimeToMinute(t);
                });
                // get working days using regex 
                days = (data.match(/[a-z-]+/ig).filter(y => y.length >= 3));
                days.forEach(day => {
                    // if workind day is between like mon-sun check for that particular day in between..
                    if (day.indexOf('-') !== -1) {
                        start = day.split('-')[0];
                        end = day.split('-')[1];
                        for (var i = weekDay.indexOf(start); i <= weekDay.indexOf(end); i++) {
                            if (weekDay[i] === weekDay[presentDay - 1]) {
                                if (isPresentWithinGivenTime(time, presentTime)) {
                                    restaurants.push(y);
                                }
                            }
                        }
                    } // just check the given day.. 
                    else if (day === weekDay[presentDay - 1]) {
                        if (isPresentWithinGivenTime(time, presentTime)) {
                            console.log(y);
                            restaurants.push(y);
                        }
                    }
                });
            })
        }
    }
    return restaurants;
}
// check the condition for time is it present within a given time
function isPresentWithinGivenTime(time, givenTime) {
    let openingTime = time[0];
    let closingTime = time[1];
    while (true) {
        openingTime = (openingTime + 1) % 1440;
        if (openingTime === givenTime) {
            return true;
        }
        if (openingTime === closingTime) {
            return false;
        }
    }
}

// conversion of time to minute 
function convertTimeToMinute(time) {
    var temp = 0;
    var minutesTime = 0;
    if (time.indexOf('pm') == -1 && time.indexOf('am') == -1) {
        return;
    }
    // 12:00pm and 12:00am special case..
    if (time.indexOf('pm') !== -1 && (parseInt(time.match(/[0-9:]+/ig)[0].split(':')[0])) !== 12) {
        temp = 12;
    }
    if (time.indexOf('am') !== -1 && (parseInt(time.match(/[0-9:]+/ig)[0].split(':')[0])) == 12) {
        temp = -12;
    }
    if ((time.match(/[0-9:]+/ig)[0].indexOf(':') !== -1)) {

        minutesTime = (parseInt(time.match(/[0-9:]+/ig)[0].split(':')[0]) + temp) * 60 + parseInt(time.match(/[0-9:]+/ig)[0].split(':')[1])

    } else {
        minutesTime = (parseInt(time) + temp) * 60;
    }
    return (minutesTime);
}